#include "mainwindow2.h"
#include "ui_mainwindow2.h"
#include <vector>
#include <QFileDialog>
#include <QString>
#include <QMessageBox>

MainWindow2::MainWindow2(QWidget *parent) :
    QMainWindow(parent),
    m_ui(new Ui::MainWindow2)
{
    m_player = new QCvPlayer();
    QObject::connect(m_player, SIGNAL(processedImage(QImage)),
                     this, SLOT(updatePlayerUI(QImage)));
    m_ui->setupUi(this);
    m_ui->selectCam->hide();
    QObject::connect(m_ui->selectVid, SIGNAL(pressed()),this, SLOT(loadButton_clicked()));
    QObject::connect(m_ui->radioButton_Camera,SIGNAL(toggled(bool)),this,SLOT(radioButton_Camera_toggled(bool)));
    QObject::connect(m_ui->radioButton_Video,SIGNAL(toggled(bool)),this,SLOT(radioButton_Video_toggled(bool)));
    QObject::connect(m_ui->pushButton_Play, SIGNAL(pressed()),this, SLOT(playButton_clicked()));
    QObject::connect(m_ui->pushButton_Pause, SIGNAL(pressed()),this, SLOT(playButton_clicked()));
    QObject::connect(m_ui->horizontalSlider_Calibration, SIGNAL(valueChanged(int)),this,SLOT(calibrationValueChange(int)));
    QObject::connect(m_ui->doubleSpinBox_Calibration,SIGNAL(valueChanged(double)),this,SLOT(calibrationValueChange(double)));

    init_UI_hidden();
    select_video_camera=0;
}

MainWindow2::~MainWindow2()
{
    delete m_player;
    delete m_ui;
}

void MainWindow2::updatePlayerUI(QImage img)
{
    if (!img.isNull())
    {
        m_ui->Videozone1->setAlignment(Qt::AlignCenter);
        m_ui->Videozone1->setPixmap(QPixmap::fromImage(img).scaled(m_ui->Videozone1->size(),
                                                              Qt::KeepAspectRatio, Qt::FastTransformation));
    }
}

void MainWindow2::loadButton_clicked()
{
    QString filename = QFileDialog::getOpenFileName(this,
                                                    tr("Open Video"), ".",
                                                    tr("Video Files (*.avi *.mpg *.mp4 *.mkv)"));
    if (!filename.isEmpty())
    {
        if (!m_player->loadVideo(filename.toLocal8Bit().data()))
        {
            QMessageBox msgBox;
            msgBox.setText(tr("The selected video could not be opened!"));
            msgBox.exec();
        }
    }
    else
    {
        QMessageBox msgBox;
        msgBox.setIcon(QMessageBox::Critical);
        msgBox.setWindowTitle(tr("Filename Empty"));
        msgBox.setText(tr("The filename was Empty. Please select a video"));
        msgBox.exec();
    }
}

void MainWindow2::playButton_clicked()
{
    // If we are on video mode and video is selected
    if(select_video_camera==0 && !m_player->getActualFilename().trimmed().isEmpty())
    {

        if (m_player->isStopped())
        {
            m_player->play();
            m_ui->pushButton_Pause->setText(tr("Stop"));
        }
        else
        {
            m_player->stop();
            m_ui->pushButton_Play->setText(tr("Play"));
        }
    }
    //If we are on camera mode
    else if(select_video_camera==1)
    {


        if (m_player->isStopped())
        {
            m_player->loadCamera(getSelectedCamera());
            m_player->play();
            m_ui->pushButton_Play->setText(tr("Stop"));
        }
        else
        {
            m_player->stop();
            m_ui->pushButton_Play->setText(tr("Play"));
        }
    }
    //If there isn't video selected and not in camera mode
    else
    {
        m_ui->pushButton_Play->setText(tr("Play"));
        QMessageBox msgBox;
        msgBox.setWindowTitle(tr("No video selected"));
        msgBox.setText(tr("No video currently selected, please, select one first."));
        msgBox.exec();
    }

}

void MainWindow2::replayButton_clicked()
{
    m_player->reset();
    if(!m_player->isRunning())
    {
        playButton_clicked();
    }
}

void MainWindow2::radioButton_Camera_toggled(bool value){
    if(value==true)
    {
        select_video_camera=1;
        m_ui->pushButton_Play->setText(tr("Play Camera"));
        m_player->stop();
        m_player->loadCamera(getSelectedCamera());
    }
}

void MainWindow2::radioButton_Video_toggled(bool value){
    if(value==true)
    {
        select_video_camera=0;
        m_ui->pushButton_Play->setText(tr("Play Video"));
        m_player->stop();
        m_player->loadVideo(m_player->getActualFilename());
    }
}

int MainWindow2::getSelectedCamera(){
    return m_ui->spinBox_2->value();
}

QString MainWindow2::boolToString(bool x){
    if(x==1){
        return "true";
    }else{
        return "false";
    }
}

void MainWindow2::calibrationValueChange(int v){
   calibrationValueChange((double) v);
}
void MainWindow2::calibrationValueChange(double v){
   calibrationValue=v;
   m_ui->doubleSpinBox_Calibration->setValue(v);
   m_ui->horizontalSlider_Calibration->setValue((int)v);
}

void MainWindow2::portValueChanged(unsigned int v){
    lo_address_free(receiverAddress);
    std::string s = std::to_string(v);
    const char* portAsChar= s.c_str();
    receiverAddress=lo_address_new("127.0.0.1",portAsChar);
}

void MainWindow2::init_UI_hidden(){
    m_ui->pushButton_ReloadCameras->hide();
    m_ui->selectCam->hide();
}
