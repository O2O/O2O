#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QFileDialog>
#include <QString>
#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    m_ui(new Ui::MainWindow)
{
    m_player = new QCvPlayer();
    QObject::connect(m_player, SIGNAL(processedImage(QImage)),
                     this, SLOT(updatePlayerUI(QImage)));
    m_ui->setupUi(this);
    m_ui->numeroCamera->hide();
}

MainWindow::~MainWindow()
{
    delete m_player;
    delete m_ui;
}

void MainWindow::updatePlayerUI(QImage img)
{
    if (!img.isNull())
    {
        m_ui->label->setAlignment(Qt::AlignCenter);
        m_ui->label->setPixmap(QPixmap::fromImage(img).scaled(m_ui->label->size(),
                                                              Qt::KeepAspectRatio, Qt::FastTransformation));
    }
}

void MainWindow::on_loadButton_clicked()
{
    QString filename = QFileDialog::getOpenFileName(this,
                                                    tr("Open Video"), ".",
                                                    tr("Video Files (*.avi *.mpg *.mp4 *.mkv)"));
    if (!filename.isEmpty())
    {
        if (!m_player->loadVideo(filename.toLocal8Bit().data()))
        {
            QMessageBox msgBox;
            msgBox.setText(tr("The selected video could not be opened!"));
            msgBox.exec();
        }
    }
    else
    {
        QMessageBox msgBox;
        msgBox.setIcon(QMessageBox::Critical);
        msgBox.setWindowTitle(tr("Filename Empty"));
        msgBox.setText(tr("The filename was Empty. Please select a video"));
        msgBox.exec();
    }
}

void MainWindow::on_playButton_clicked()
{
     // If we are on video mode and video is selected
    if(select_video_camera==0 && !m_player->getActualFilename().trimmed().isEmpty())
    {

        if (m_player->isStopped())
        {
            m_player->play();
            m_ui->playButton->setText(tr("Stop"));
        }
        else
        {
            m_player->stop();
            m_ui->playButton->setText(tr("Play"));
        }
    }
    //If we are on camera mode
    else if(select_video_camera==1)
    {

        if (m_player->isStopped())
        {
            m_player->loadCamera(getSelectedCamera());
            m_player->play();
            m_ui->playButton->setText(tr("Stop"));
        }
        else
        {
            m_player->stop();
            m_ui->playButton->setText(tr("Play"));
        }
    }
    //If there isn't video selected and not in camera mode
    else
    {
        m_ui->playButton->setText(tr("Play"));
        QMessageBox msgBox;
        msgBox.setWindowTitle(tr("No video selected"));
        msgBox.setText(tr("No video currently selected, please, select one first."));
        msgBox.exec();
    }

}

void MainWindow::on_replayButton_clicked()
{
    m_player->reset();
    if(!m_player->isRunning())
    {
        on_playButton_clicked();
    }
}

void MainWindow::on_radioButton_Camera_toggled(bool value){
    if(value==true)
    {
        m_ui->playButton->setText(tr("Play Camera"));
        m_player->stop();
        select_video_camera=1;
        m_player->loadCamera(getSelectedCamera());
    }
}

void MainWindow::on_radioButton_Video_toggled(bool value){
    if(value==true)
    {
        m_ui->playButton->setText(tr("Play Video"));
        select_video_camera=0;
        m_player->stop();
        m_player->loadVideo(m_player->getActualFilename());
    }
}

int MainWindow::getSelectedCamera(){
    return m_ui->numeroCamera->value();
}
