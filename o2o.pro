#-------------------------------------------------
#
# Project created by QtCreator 2017-07-14T10:46:18
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
QMAKE_CXXFLAGS += -std=c++11

TARGET = testwidget
TEMPLATE = app

SOURCES += main.cpp\
        mainwindow.cpp \
        qcvplayer.cpp \
        mainwindow2.cpp

HEADERS += mainwindow.h \
        qcvplayer.h \
        mainwindow2.h

FORMS   += mainwindow.ui\
        mainwindow2.ui

# OS specific directives
# Please likely duplicate than merging OSs
win32 {
    message("Using win32 configuration")
    #change those 2 lines According to YOUR configuration
    #This one is where Opencv build Is
    OPENCV_PATH = D:/ocv
    #This one one is where the Headers are (In the opencv source folder your extracted)
    INCLUDEPATH += D:/opencv/opencv/build/include
    #This is Where your libraries compiled with MinGW should be, you probably don't have to change anything here.
    LIBS_PATH = "$$OPENCV_PATH/bin"

    #Change the libraries in function of your version
    CONFIG(debug, debug|release) {
        LIBS     += -L$$LIBS_PATH \
                    -lopencv_core2413 \
                    -lopencv_highgui2413 \
                    -lopencv_imgproc2413
    }

    CONFIG(release, debug|release) {
        LIBS     += -L$$LIBS_PATH \
                    -lopencv_core2413 \
                    -lopencv_highgui2413 \
                    -lopencv_imgproc2413
    }

    RC_ICONS = icon.ico
}

unix {
    message("Using unix configuration")

    LIBS += -llo

    WITH_LIBV4L=true

    OPENCV_PATH = /usr/include/opencv2/

    LIBS    +=  -lopencv_core \ # -L$$LIBS_PATH \
                -lopencv_highgui \
                -lopencv_imgproc

    INCLUDEPATH += $$OPENCV_PATH/modules/core/include/ \ #core module
        $$OPENCV_PATH/modules/highgui/include/ #highgui module
}

macx {
    message("Using macx configuration")

    # TODO define an OSX version target, what is actualy supported? 10.7 ???
    QMAKE_MACOSX_DEPLOYMENT_TARGET = 10.4
    QMAKE_MAC_SDK = /Developer/SDKs/MacOSX10.4u.sdk
    CONFIG += app_bundle
    CONFIG += x86 ppc

    LIBS    +=  -framework IOKit \
                -framework Carbon \
                -framework Cocoa \
                -framework System \
                -framework QuickTime \
                -framework AGL \
                -lpthread \
                -lssl \
                -liconv
    INCLUDEPATH +=


    LibsFiles.files = $$PROJECTS_LIBS_PATH/libBBB.1.dylib $$PROJECTS_LIBS_PATH/libAAA.1.dylib \
$$PROJECTS_LIBS_PATH/libXXX.1.dylib $$PROJECTS_LIBS_PATH/libYYYr.1.dylib #TODO
    LibsFiles.path = Contents/Frameworks #TODO
    QMAKE_BUNDLE_DATA += LibsFiles

    QMAKE_INFO_PLIST = "O2O.plist" #TODO
    ICON = icon.icns #TODO
}

# DEBUG for compilation
message("OpenCV path: $$OPENCV_PATH")
message("Includes path: $$INCLUDEPATH")
message("Libraries: $$LIBS")

DISTFILES +=

RESOURCES += \
    ressources.qrc


