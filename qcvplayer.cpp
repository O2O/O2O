#include "qcvplayer.h"

QCvPlayer::QCvPlayer(QObject *parent) :
    QThread(parent),
    m_stop(true),
    m_actualFilename("")
{

}

bool QCvPlayer::loadVideo(QString filename)
{
    m_capture.open(filename.toLocal8Bit().data());
    m_actualFilename=filename;
    if (m_capture.isOpened())
    {
        m_frameRate = static_cast<int> (m_capture.get(CV_CAP_PROP_FPS));
        return true;
    }
    else
        return false;
}

bool QCvPlayer::loadCamera(int id)
{
    m_capture.open(id);
    if (m_capture.isOpened())
    {
        m_frameRate = static_cast<int> (m_capture.get(CV_CAP_PROP_FPS));
        return true;
    }
    else
        return false;
}

void QCvPlayer::play()
{
    if (!isRunning())
    {
        if (isStopped())
        {
            m_stop = false;
        }
        start(LowPriority);
    }
}

void QCvPlayer::run()
{
    int delay = (1000/m_frameRate);
    while(!m_stop)
    {
        if (!m_capture.read(m_frame))
        {
            m_stop = true;
        }

        if (m_frame.channels()== 3)
        {
            cv::cvtColor(m_frame, m_RGBframe, CV_BGR2RGB);
            m_img = QImage(static_cast<const unsigned char*>(m_RGBframe.data),
                           m_RGBframe.cols,m_RGBframe.rows,QImage::Format_RGB888);
        }
        else
        {
            m_img = QImage(static_cast<const unsigned char*>(m_frame.data),
                           m_frame.cols,m_frame.rows,QImage::Format_Indexed8);
        }
        emit processedImage(m_img);
        millisleep(delay);
    }
}

QCvPlayer::~QCvPlayer()
{
    m_mutex.lock();
    m_stop = true;
    m_capture.release();
    m_condition.wakeOne();
    m_mutex.unlock();
    wait();
}

void QCvPlayer::stop()
{
    m_stop = true;
}

void QCvPlayer::millisleep(int ms)
{
    struct timespec ts = { ms / 1000, (ms % 1000) * 1000 * 1000 };
    nanosleep(&ts, NULL);
}

bool QCvPlayer::isStopped() const
{
    return m_stop;
}

void QCvPlayer::reset()
{
    loadVideo(m_actualFilename);
}

QString QCvPlayer::getActualFilename()
{
    return m_actualFilename;
}
