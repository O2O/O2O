#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
This script perform blob detection with kinect and openCV and send values
via OpenSoundControl
"""

import cv2 # openCV
from get_kinect import get_video, get_depth
import numpy as np

def send_osc(path, message):
    """ send an OSC message on local machine, port 9000 """
    liblo.send(('127.0.0.1', 9000), '/kinect/'+path, message)

def send_one_point(osc_path, point):
    """ send a point with OSC """
    send_osc(osc_path+'/x', float(point[0]))
    send_osc(osc_path+'/y', float(point[1]))

def get_extreme_points(c):
    """ return blob extremes values """
    left = tuple(c[c[:, :, 0].argmin()][0])
    right = tuple(c[c[:, :, 0].argmax()][0])
    top = tuple(c[c[:, :, 1].argmin()][0])
    bot = tuple(c[c[:, :, 1].argmax()][0])
    return left, right, top, bot

def draw_extreme_points(image, points, color):
    """ draw circle on image (to see where extreme points are) """
    cv2.circle(image, points[0], 8, color, -1)
    cv2.circle(image, points[1], 8, color, -1)
    cv2.circle(image, points[2], 8, color, -1)
    cv2.circle(image, points[3], 8, color, -1)

if __name__ == "__main__":
    # With freenet, the first images are dirty. So, do nothing with first ones
    for i in range(30):
        get_depth()

    # mask is usefull to ignore custom areas (same concept as Photoshop or Gimp).
    mask = cv2.imread('mask_fresnoy_grande_nef.png')
    mask = cv2.cvtColor(mask, cv2.COLOR_RGB2GRAY)

    # Background subtraction. History means number of images used to perform it.
    # varThreshold is a important value to calibrate it.
    fgbg = cv2.BackgroundSubtractorMOG2(
        history=500,
        varThreshold = 0.7,
        bShadowDetection = False
        )

    # kernel will be used to perform erosion the dilatation (noise filter).
    # In this case, we choose 5px, but this parameter is an important choice
    # to calibrate the detection according to the image noise ratio.
    kernel = np.ones((5, 5),np.uint8)

    # Minimum area of a blob. Under this value, we consider it's a glitch
    MINIMUM_AREA_SIZE = 1000

    # Python dict to contain blob points
    points = {}

    # Main loop
    while 1:
        frame = get_depth()

        bgsub = fgbg.apply(frame) # background substraction
        bgsub = cv2.bitwise_and(bgsub, mask) # apply mask

        # filter : OPEN = erosion then dilatation
        bgsub = cv2.morphologyEx(bgsub, cv2.MORPH_OPEN, kernel)

        # We copy a color image for the visualisation
        color_im = cv2.cvtColor(bgsub, cv2.COLOR_GRAY2RGB)

        # findContours is the main openCV algorithm that detect blobs.
        # See the doc ;)
        contours, hier = cv2.findContours(bgsub.copy(), 1, 2)

        # Draw contours on image to visualise it.
        cv2.drawContours(color_im, contours, -1, (0, 255, 0), 3)

        i = 0 # nbr of blobs
        total_size = 0 # sum of all areas
        for contour in contours:
            m = cv2.moments(contour) #
            if m['m00'] > MINIMUM_AREA_SIZE: # m['m00'] means surface area
                i += 1 # Yes: we have a real blob :p

                # perform his centroïd
                cx = int(m['m10']/m['m00'])
                cy = int(m['m01']/m['m00'])
                print i, cx, cy, m['m00']

                # send centroïd in OSC
                send_osc('contours/'+str(i)+'/x', cx)
                send_osc('contours/'+str(i)+'/y', cy)
                send_osc('contours/'+str(i)+'/size', m['m00'])
                total_size += m['m00']

                # Display the blob number on the video
                cv2.putText(
                    color_im,
                    str(i),
                    (cx, cy),
                    cv2.FONT_HERSHEY_PLAIN, 2, (255, 0, 0))

                # perform and send extreme values of each blob
                points = get_extreme_points(contour)
                send_one_point('contours/'+str(i)+'/ext_left', points[0])
                send_one_point('contours/'+str(i)+'/ext_right', points[1])
                send_one_point('contours/'+str(i)+'/ext_top', points[2])
                send_one_point('contours/'+str(i)+'/ext_bottom', points[3])

                # display if on the video with a random color
                color = (64 * (i -1) % 255, 0, 64 * i % 255)
                draw_extreme_points(color_im, points, color)

        # send the sum of detected blobs areas
        send_osc('contours/global/size', total_size)

        # Show the video
        cv2.imshow('Kinect', color_im)
        key = cv2.waitKey(10) # esc key
        if key == 27:
            break
