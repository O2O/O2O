#!/usr/bin/python

"""
Retrive Kinect images via Freenect driver and convert it in good OpenCV format
"""

import freenect # Kinect driver
import cv2 # OpenCV
import numpy as np # numpy is array manipulation library for python

def get_video():
    array, _ = freenect.sync_get_video()
    array = cv2.cvtColor(array,cv2.COLOR_RGB2BGR)
    return array
             
def get_depth():
    array, _ = freenect.sync_get_depth()
    array = array.astype(np.uint8)
    return array

if __name__ == "__main__":
    while 1:
        cv2.imshow('RGB image', get_video())
        cv2.imshow('Depth image', get_depth())
        # quit program when 'esc' key is pressed
        k = cv2.waitKey(5) & 0xFF
        if k == 27:
            break

