#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include "qcvplayer.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    QImage m_imageDisplay;  //This will create QImage which is shown in Qt label
    int getSelectedCamera();

private:
    Ui::MainWindow *m_ui;
    QCvPlayer *m_player;
    bool select_video_camera;

private slots:
    //Display video frame in UI player
    void updatePlayerUI(QImage img);
    //Slot for the load video push button.
    void on_loadButton_clicked();
    // Slot for the play push button.
    void on_playButton_clicked();
    // Slot for the reset button push
    void on_replayButton_clicked();
    // Slot for the Video radio toogled
    void on_radioButton_Video_toggled(bool);
    // Slot for the Cameda radio toogled
    void on_radioButton_Camera_toggled(bool);
};

#endif // MAINWINDOW_H

