#ifndef QCVPLAYER_H
#define QCVPLAYER_H

#include <QMutex>
#include <QThread>
#include <QImage>
#include <QString>
#include <QWaitCondition>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

class QCvPlayer : public QThread
{
    Q_OBJECT
public:
    explicit QCvPlayer(QObject *parent = 0);
    ~QCvPlayer();
    //Load a video from memory
    bool loadVideo(QString filename);
    //Play the video
    void play();
    //Stop the video
    void stop();
    //check if the player has been stopped
    bool isStopped() const;
    //reset video
    void reset();
    //get Filename
    QString getActualFilename();
    //Load camera
    bool loadCamera(int id);

signals:
    void frameReady(cv::Mat);
    //Signal to output frame to be displayed
    void processedImage(const QImage &image);

public slots:

protected:
    void run();
    void millisleep(int ms);

private:
    bool m_stop;
    QMutex m_mutex;
    QWaitCondition m_condition;
    cv::Mat m_frame;
    int m_frameRate;
    cv::VideoCapture m_capture;
    cv::Mat m_RGBframe;
    QImage m_img;
    QString m_actualFilename;
};

#endif // QCVPLAYER_H
