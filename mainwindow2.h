#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "qcvplayer.h"
#include <lo/lo.h>
#include <lo/lo_cpp.h>
#include <lo/lo_errors.h>
#include <vector>

namespace Ui {
class MainWindow2;
}

class MainWindow2 : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow2(QWidget *parent = 0);
    ~MainWindow2();

    QImage m_imageDisplay1;  //This will create QImage which is shown in Qt label
    QImage m_imageDisplay2;
    int getSelectedCamera();

private:
    Ui::MainWindow2 *m_ui;
    QCvPlayer *m_player;
    bool select_video_camera;
    std::vector<int> listecam;
    bool EnumerateCameras(std::vector<int> &camIdx);
    QString boolToString(bool);
    double calibrationValue;
    lo::Address receiverAddress=lo_address_new("localhost","9000");
    void init_UI_hidden();

private slots:
    //Display video frame in UI player
    void updatePlayerUI(QImage img);
    //Slot for the load video push button.
    void loadButton_clicked();
    // Slot for the play push button.
    void playButton_clicked();
    // Slot for the reset button push
    void replayButton_clicked();
    // Slot for the Video radio toogled
    void radioButton_Video_toggled(bool);
    // Slot for the Cameda radio toogled
    void radioButton_Camera_toggled(bool);
    // Slot for the Change the slider Calibration
    void calibrationValueChange(int value);
    void calibrationValueChange(double value);
    // Slot for achange in port number
    void portValueChanged(unsigned int portValue);

};

#endif // MAINWINDOW_H

